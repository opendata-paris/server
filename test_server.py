import io
from flask import Flask
from unittest import TestCase
from server import app

class TestViews(TestCase):

    def setUp(self):
        self.app = app.test_client()

    def test_getDataEmpty(self):
        response = self.app.get('/data')
        self.assertEqual(response.json, [])

    def test_uploadFile(self):
        with open('./upload/espaces_verts.csv', 'rb') as file:
            fileIO = io.BytesIO(file.read())
            response = self.app.post(
                '/file', 
                buffered=True,
                content_type='multipart/form-data', 
                data={'file': ((fileIO), 'espaces_vers.csv')}
            )
            fileIO.close()
            file.close()

        self.assertEqual(response.status, '200 OK')
    
    def test_getDataFull(self):
        with open('./upload/espaces_verts.csv', 'rb') as file:
            fileIO = io.BytesIO(file.read())
            response = self.app.post(
                '/file', 
                buffered=True,
                content_type='multipart/form-data', 
                data={'file': ((fileIO), 'testfield.csv')})
            fileIO.close()
            file.close()
        
        response = self.app.get('/data')
            
        self.assertEqual(response.status, '200 OK')
        self.assertTrue(isinstance(response.json, list))
