import os, csv, sqlite3, sys, json

from flask import Flask, flash, request, redirect, url_for
from werkzeug.utils import secure_filename
from flask_cors import CORS, cross_origin

app = Flask(__name__)
cors = CORS(app)
conn = sqlite3.connect(':memory:', check_same_thread=False)

UPLOAD_FOLDER = './upload'
ALLOWED_EXTENSIONS = {'csv'}

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

csv.field_size_limit(sys.maxsize)

c = conn.cursor()
c.execute('''
    CREATE TABLE donnees (
        id INT PRIMARY KEY, 
        name TEXT, 
        adress TEXT
    )
''')

conn.commit()

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/file', methods=['POST'])
@cross_origin()
def upload_file():
    if 'file' not in request.files:
        return 'No file part'
    file = request.files['file']
    # if user does not select file, browser also
    # submit an empty part without filename
    if file.filename == '':
        flash('No selected file')
        return 'no file'
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        with open(app.config['UPLOAD_FOLDER'] + '/' + filename, newline='') as filecsv:
            reader = csv.DictReader(filecsv, delimiter=';')
            cursor = conn.cursor()
            successCount = 0
            errorCount = 0
            for row in reader:
                if (row['Identifiant espace vert'].isnumeric()):
                    cursor.execute(
                        '''
                            REPLACE INTO donnees VALUES (
                                ?, ?, ?
                            )
                        ''',
                        (
                            row['Identifiant espace vert'], 
                            row['Nom de l\'espace vert'],
                            row['Adresse - Numero'] + ' ' + row['Adresse - type voie'] + ' ' + row['Adresse - Libelle voie'] + ' ' + row['Adresse - Complement'] + ', ' + row['Code postal']
                        )    
                    )
                    successCount = successCount + 1
                else:
                    errorCount = errorCount + 1
            conn.commit()
        return {"status": 200, "data": {"successCount": successCount, "errorCount": errorCount}}, 200, {'Content-Type': 'application/json'}

@app.route('/data', methods=['GET'])
@cross_origin()
def get_data():
    cursor = conn.cursor()

    cursor.execute('''
        SELECT * 
        FROM donnees
        ORDER BY id
    ''')

    return json.dumps([tuple(row) for row in cursor.fetchall()]), 200, {'Content-Type': 'application/json'}