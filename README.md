<h1 align="center">Welcome to Project API Paris 👋</h1>
<p>
  <a href="#" target="_blank">
    <img alt="License: MIT" src="https://img.shields.io/badge/License-MIT-yellow.svg" />
  </a>
</p>

> Back du site de collecte de la plateforme open data

### 🏠 [Homepage](https://gitlab.com/opendata-paris/server)


## Télécharger Python
https://www.python.org/downloads/

## Paramétrage environnement pour linux

```sh
export FLASK_APP=server.py
export FLASK_ENV=DEVELOPMENT
```

## Installation de Flask et lancement de l'application
Pour installer les dépendances de l'application :

```sh
pip install Flask
```

Pour lancer l'application
```sh
flask run 
```


## Author

* 👤 **Baptiste Bivaud**
* 👤 **Arthur Bosshardt**
* 👤 **Quentin Ganacheau**
* 👤 **Johann Medjo**
* 👤 **Théo Viardin**

## Show your support

Give a ⭐️ if this project helped you!

***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_